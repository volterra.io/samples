# Volterra Demo & App Examples

Group Volterra public demo & apps


**Hello Kuberentes App - samples/hello-k8s**
- Source: https://github.com/paulbouwer/hello-kubernetes

**Online Boutique (was Hipster Shop) - samples/hipster-shop**
- Source: https://github.com/GoogleCloudPlatform/microservices-demo

**/patio - a social gathering of various demos, use-cases, examples and useful scripts. 